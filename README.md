# SensorTag-Design-Files

Design files for the TI SensorTag, e.g. 3D models and STLs of the housing for 3D printing.

The parts of the SensorBox housing (base box and lid) are derived from the official TI CC2650STK files provided by TI:
 * CC2650 SensorTag 3D Files (Rev. A) SWRR138A.ZIP https://www.ti.com/lit/zip/swrr138

Original versions: Texas Instruments, 2015
 
Derived versions: Albrecht Kurze, TU Chemnitz, 2017-2022
The terms, conditions and licenses of the official TI files apply.


Newly created design files: Albrecht Kurze, TU Chemnitz, 2017-2022, CC-BY-NC